# grupo8

Projeto de PTI e PTR - grupo8

## Installation
Before running the project you have to install: <br />
	crispy - https://www.techiediaries.com/django-form-bootstrap/ <br />
	cleanup	- https://pypi.org/project/django-cleanup/ <br />
	PIL - python3 -m pip install --upgrade pip, python3 -m pip install --upgrade Pillow <br />
	sudo apt-get install libapache2-mod-wsgi-py3<br />
	pip3 install djangorestframework<br />
	pip3 install django-allauth<br />
	pip3 install locust<br />
	pip3 install django-filter<br />


Go to /etc/apache2/sites-available and change django.conf to: <br />

<VirtualHost *:80><br />
    ServerName grupo08.tk<br />
    ServerAdmin admin@localhost<br />
    DocumentRoot /var/www/grupo8producao1<br />
    ErrorLog /var/log/apache2/django_error.log<br />
    CustomLog /var/log/apache2/django_access.log combined<br />

    Alias /products_pics /var/www/grupo8producao1/media/products_pics<br />
    <Directory /var/www/grupo8producao1/media/products_pics><br />
        Require all granted<br />
    </Directory><br />

    Alias /profile_pics /var/www/grupo8producao1/media/profile_pics<br />
    <Directory /var/www/grupo8producao1/media/profile_pics><br />
        Require all granted<br />
    </Directory><br />

    Alias /media /var/www/grupo8producao1/media<br />
    <Directory /var/www/grupo8producao1/media><br />
        Require all granted<br />
    </Directory><br />

    Alias /static /var/www/grupo8producao1/main_app/static<br />
    <Directory /var/www/grupo8producao1/main_app/static><br />
        Require all granted<br />
    </Directory><br />

    <Directory /var/www/grupo8producao1/grupo8><br />
        <Files wsgi.py><br />
            Require all granted<br />
        </Files><br />
    </Directory><br />
    WSGIDaemonProcess grupo8producao1 python-path=/var/www/grupo8producao1/<br />
    WSGIProcessGroup grupo8producao1<br />
    WSGIScriptAlias / /var/www/grupo8producao1/grupo8/wsgi.py<br />
</VirtualHost><br />

Go to /var/www:<br />
	git clone git@gitlab.com:fernandesdf/grupo8producao1.git<br />
	chown www-data:www-data grupo8producao1<br />
	cd grupo8producao1<br />
	chown www-data:www-data db.sqlite3<br />
	chown www-data:www-data media<br />
	cd media<br />
	chown www-data:www-data profile_pics<br />
	chown www-data:www-data products_pics<br />



## Authors and acknowledgment
André Pinto N48722 <br />
Tiago Garcia N54938 <br />
Diogo Fernandes N54967 <br />
Diogo Emidio N54969 <br />
Raphael Marques N55135 <br />
Afonso Gama N55857 <br />
