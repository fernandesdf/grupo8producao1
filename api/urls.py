"""grupo8 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from main_app import views as main_views
from api import views as api_view




urlpatterns = [

    path('admin/', admin.site.urls),
    path('', include('main_app.urls')),
    path('register/', main_views.register, name='register'),
    path('profile/', main_views.profile, name='profile'),
    path('profile/delete', main_views.deleteUser, name="UserDelete"),
    path('login/', main_views.login, name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='main_app/logout.html'), name='logout'),

    #REST API urls
    
    path('', api_view.getProducts, name='api'),
    path('product/', api_view.getProducts,name='getProducts'),
    path('add_product/', api_view.addProduct,name='add_product'),
    path('delete_product/', api_view.deleteProduct,name='delete_product'),  
    path('put_product/', api_view.putProduct,name='put_product'),  

    path('profile/', api_view.getProfiles,name='getProfiles'), 
    path('add_profile/', api_view.addProfile,name='add_profile'), 
    path('delete_profile/', api_view.deleteProfile,name='delete_profile'),  
    path('put_profile/', api_view.putProfile,name='put_profile'),  

    path('pollution/', api_view.getPollution,name='poluicao'),
    path('pollution/addpollution', api_view.addPollution,name='addpoluicao'),
    path('pollution/delpollution/<slug>', api_view.DeletePollution,name='deletepoluicao'),

    path('resource/', api_view.getResources,name='resources'),
    path('resource/addresource', api_view.addResource,name='addresource'),
    path('resource/deleteresource/<slug>', api_view.DeleteResource,name='deleteresource'),

    path('users/type', api_view.getUserType,name='users-type'),
    path('users/type/add', api_view.addUserType,name='add_users-type'),
    path('users/type/delete/<slug>', api_view.deleteUserType,name='delete_users-type'),
    path('users/type/put/<slug>', api_view.putUserType,name='put_users-type'),

    path('users/', api_view.getCustomUser,name='users'),
    path('users/add/', api_view.addCustomUser,name='add_users'),
    path('users/delete/<slug>', api_view.deleteCustomUser,name='delete_users'),
    path('users/put/<slug>', api_view.putCustomUser,name='put_users'),   

    path('category/', api_view.getCategory,name='getCategory'),
    path('add_category/', api_view.addCategory,name='addCategory'),
    path('category/delcategory/<slug>', api_view.deleteCategory,name='deleteCategory'),

    path('subcategory/', api_view.getSubCategory,name='getSubCategory'),
    path('add_subcategory/', api_view.addSubCategory,name='addSubCategory'),
    path('subcategory/delsubcategory/<slug>', api_view.deleteSubCategory,name='deleteSubCategory'),

    path('product/category/', api_view.getProductCategory,name='getProductCategory'),  
    path('product/add/category/', api_view.addProductCategory,name='addProductCategory'),  
    path('product/delete/category/<slug>', api_view.deleteProductCategory,name='deleteProductCategory'),  

    path('product/subcategory/', api_view.getProductSubCategory,name='getProductSubCategory'),  
    path('product/add/subcategory', api_view.addProductSubCategory,name='addProductSubCategory'),  
    path('product/delete/subcategory/<slug>', api_view.deleteProductSubCategory,name='deleteProductSubCategory'),  

    path('transporterbase/', api_view.getTransporterBase,name='get_transporterBase'),  
    path('transporterbase/add', api_view.addTransporterBase,name='add_transporterBase'),  
    #path('transporterbase/put/<slug>', api_view.putTransporterBase,name='put_TransporterBase'),   
    path('transporterbase/delete/<slug>', api_view.deleteTransporterBase,name='delete_TransporterBase'),  

    path('fuel/', api_view.getFuel,name='get_fuel'),  
    path('fuel/add', api_view.addFuel,name='add_fuel'),  
    #path('fuel/put/<slug>', api_view.putFuel,name='put_fuel'),   
    path('fuel/delete/<slug>', api_view.deleteFuel,name='delete_fuel'),  

    path('vehicle/', api_view.getVehicle,name='get_vehicle'),  
    path('vehicle/add', api_view.addVehicle,name='add_vehicle'),  
    #path('vehicle/put/<slug>', api_view.putVehicle,name='put_vehicle'),   
    path('vehicle/delete/<slug>', api_view.deleteVehicle,name='delete_vehicle'),  

    path('transport/', api_view.getTransport,name='get_transport'),  
    path('transport/add', api_view.addTransport,name='add_transport'),  
    #path('transport/put/<slug>', api_view.putTransport,name='put_transport'),   
    path('transport/delete/<slug>', api_view.deleteTransport,name='delete_transport'),  

    path('pollutionStorage/', api_view.getPollutionStorage,name='get_pollutionStorage'),  
    path('pollutionStorage/add', api_view.addPollutionStorage,name='add_pollutionStorage'),  
    #path('pollutionStorage/put/<slug>', api_view.putPollutionStorage,name='put_pollutionStorage'),   
    path('pollutionStorage/delete/<slug>', api_view.deletePollutionStorage,name='delete_pollutionStorage'),  


    path('pollutionProduct/', api_view.getPollutionProduct,name='get_pollutionProduct'),  
    path('pollutionProduct/add', api_view.addPollutionProduct,name='add_pollutionProduct'),  
    #path('pollutionProduct/put/<slug>', api_view.putPollutionProduct,name='put_pollutionProduct'),   
    path('pollutionProduct/delete/<slug>', api_view.deletePollutionProduct,name='delete_pollutionProduct'),  

    path('pollutionTransport/', api_view.getPollutionTransport,name='get_pollutionTransport'),  
    path('pollutionTransport/add', api_view.addPollutionTransport,name='add_pollutionTransport'),  
    #path('pollutionTransport/put/<slug>', api_view.putPollutionTransport,name='put_pollutionTransport'),   
    path('pollutionTransport/delete/<slug>', api_view.deletePollutionTransport,name='delete_pollutionTransport'),  

    path('resourceStorage/', api_view.getResourceStorage,name='get_resourceStorage'),  
    path('resourceStorage/add', api_view.addResourceStorage,name='add_resourceStorage'),  
    #path('resourceStorage/put/<slug>', api_view.putResourceStorage,name='put_resourceStorage'),   
    path('resourceStorage/delete/<slug>', api_view.deleteResourceStorage,name='delete_resourceStorage'),  

    path('resourceProduct/', api_view.getResourceProduct,name='get_resourceProduct'),  
    path('resourceProduct/add', api_view.addResourceProduct,name='add_resourceProduct'),  
    #path('resourceProduct/put/<slug>', api_view.putResourceProduct,name='put_resourceProduct'),   
    path('resourceProduct/delete/<slug>', api_view.deleteResourceProduct,name='delete_resourceProduct'),  

    path('resourceTransport/', api_view.getResourceTransport,name='get_resourceTransport'),  
    path('resourceTransport/add', api_view.addResourceTransport,name='add_resourceTransport'),  
    #path('resourceTransport/put/<slug>', api_view.putResourceTransport,name='put_resourceTransport'),   
    path('resourceTransport/delete/<slug>', api_view.deleteResourceTransport,name='delete_resourceTransport'),  

    path('orderItem/', api_view.getOrderItem,name='get_orderItem'),  
    path('orderItem/add', api_view.addOrderItem,name='add_orderItem'),  
    #path('orderItem/put/<slug>', api_view.putOrderItem,name='put_orderItem'),   
    path('orderItem/delete/<slug>', api_view.deleteOrderItem,name='delete_orderItem'),  

    path('order/', api_view.getOrder,name='get_order'),  
    path('order/add', api_view.addOrder,name='add_order'),  
    #path('order/put/<slug>', api_view.putOrder,name='put_order'),   
    path('order/delete/<slug>', api_view.deleteOrder,name='delete_order'),  

    path('orderHistory/', api_view.getOrderHistory,name='get_orderHistory'),  
    path('orderHistory/add', api_view.addOrderHistory,name='add_orderHistory'),  
    #path('orderHistory/put/<slug>', api_view.putOrderHistory,name='put_orderHistory'),   
    path('orderHistory/delete/<slug>', api_view.deleteOrderHistory,name='delete_orderHistory'),  
 
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

