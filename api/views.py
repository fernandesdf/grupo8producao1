from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from main_app.models import *
from main_app.serializers import *



#------------------------------------TipoUser------------------------------------#
@api_view(['GET'])
def getUserType(request):
    user_type = UserType.objects.all()
    serializer = UserTypeSerializer(user_type, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addUserType(request):
    if request.method == 'POST':
        serializer = UserTypeSerializer(data=request.data)
        data = {}
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response("erro")

@api_view(['PUT'])
def putUserType(request, slug):
    try:
        user_type =  UserType.objects.get(id=slug)
    except UserType.DoesNotExit:
        return Response('O tipo de Utilizador nao existe para ser atualizado')
    
    if request.method == 'PUT':
        serializer = UserTypeSerializer(user_type, data=request.data)
        data = {}
        if serializer.is_valid():
            serializer.save()
            data["sucess"] = "tipo de Utilizador foi atualizado com sucesso"
            return Response(data=data)
        return Response(serializer.errors)

@api_view(['DELETE'])
def deleteUserType(request, slug):
    try:
        usertype =  UserType.objects.get(id=slug)
    except UserType.DoesNotExit:
        return Response('O tipo de Utilizador nao existe para ser apagado')
    
    if request.method == 'DELETE':
        operation = usertype.delete()
        data = {}
        if operation:
            data["sucess"] = "Tipo de Utilizador apagado com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "Tipo de Utilizador não foi apagado com sucesso"
        return Response(data=data)


#------------------------------------CustomUser------------------------------------#
@api_view(['GET'])
def getCustomUser(request):
    customer_user = CustomUser.objects.all()
    serializer = CustomUserserializer(customer_user, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addCustomUser(request):
    if request.method == 'POST':
        serializer = CustomUserserializer(data=request.data)
        #data = {}
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response("erro")

@api_view(['PUT'])
def putCustomUser(request, slug):
    try:
        customer_user =  CustomUser.objects.get(username=slug)
    except CustomUser.DoesNotExit:
        return Response('O tipo de Utilizador nao existe para ser atualizado')
    
    if request.method == 'PUT':
        serializer = CustomUserserializer(customer_user, data=request.data)
        data = {}
        if serializer.is_valid():
            serializer.save()
            data["sucess"] = "tipo de Utilizador foi atualizado com sucesso"
            return Response(data=data)
        return Response(serializer.errors)

@api_view(['DELETE'])
def deleteCustomUser(request, slug):
    try:
        customUser =  CustomUser.objects.get(username=slug)
    except CustomUser.DoesNotExit:
        return Response('O Utilizador nao existe para ser apagado')
    
    if request.method == 'DELETE':
        operation = customUser.delete()
        data = {}
        if operation:
            data["sucess"] = "Utilizador apagado com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "Utilizador não foi apagado com sucesso"
        return Response(data=data)



#---------------------------------------Produtos----------------------------------------------------------#

@api_view(['GET'])
def getProducts(request):
    product = Product.objects.all()
    serializer = ProductSerializer(product, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addProduct(request):
    if request.method == 'POST':
        serializer = ProductSerializer(data=request.data)
        #data = {}
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response("erro")

@api_view(['PUT'])
def putProduct(request, slug):
    try:
        product =  Product.objects.get(slug=slug)
    except Product.DoesNotExit:
        return Response('O produto nao existe para ser atualizado')
    
    if request.method == 'PUT':
        serializer = ProductSerializer(product, data=request.data)
        data = {}
        if serializer.is_valid():
            serializer.save()
            data["sucess"] = "Produto foi atualizado com sucesso"
            return Response(data=data)
        return Response(serializer.errors)

@api_view(['DELETE'])
def deleteProduct(request, slug):
    try:
        product =  Product.objects.get(slug=slug)
    except Product.DoesNotExit:
        return Response('O produto nao existe para ser apagado')
    
    if request.method == 'DELETE':
        operation = product.delete()
        data = {}
        if operation:
            data["sucess"] = "Produto apagado com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "Produto não foi apagado com sucesso"
        return Response(data=data)


#------------------------------------Perfil------------------------------------#

@api_view(['GET'])
def getProfiles(request):
    if request.method == 'GET':
        perfil = ProfileSerializer.objects.all()
        serializer = ProfileSerializer(perfil)
    return Response(serializer.data)

@api_view(['POST'])
def addProfile(request):
    if request.method == 'POST':
        serializer = ProfileSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['PUT'])
def putProfile(request, slug):
    try:
        profile =  Profile.objects.get(user=slug)
    except Profile.DoesNotExit:
        return Response('O produto nao existe para ser atualizado')
    
    if request.method == 'PUT':
        serializer = ProfileSerializer(profile, data=request.data)
        data = {}
        if serializer.is_valid():
            serializer.save()
            data["sucess"] = "Produto foi atualizado com sucesso"
            return Response(data=data)
        return Response(serializer.errors)

@api_view(['DELETE'])
def deleteProfile(request, slug):
    try:
        profile =  Profile.objects.get(user=slug)
    except Profile.DoesNotExit:
        return Response('A poluicao nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = profile.delete()
        data = {}
        if operation:
            data["sucess"] = "Poluicao apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "Poluicao não foi apagada com sucesso"
        return Response(data=data)
        
#fields= ['user', 'image', 'address', 'cell_phone', 'nif']

#------------------------------------Poluicao------------------------------------#
@api_view(['GET'])
def getPollution(request):
    if request.method == 'GET':
        poluicao = Pollution.objects.all()
        serializer = PollutionSerializer(poluicao, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addPollution(request):
    if request.method == 'POST':
        serializer = PollutionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
    #return Response(serializer.data)

@api_view(['DELETE'])
def DeletePollution(request, slug):
    try:
        product =  Pollution.objects.get(name=slug)
    except Pollution.DoesNotExit:
        return Response('A poluicao nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = product.delete()
        data = {}
        if operation:
            data["sucess"] = "Poluicao apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "Poluicao não foi apagada com sucesso"
        return Response(data=data)


#------------------------------------Recursos------------------------------------#

@api_view(['GET'])
def getResources(request):
    if request.method == 'GET':
        recursos = Resource.objects.all()
        serializer = ResourceSerializer(recursos, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addResource(request):
    if request.method == 'POST':
        serializer = ResourceSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def DeleteResource(request, slug):
    try:
        resource =  Resource.objects.get(name=slug)
    except Resource.DoesNotExit:
        return Response('O recurso nao existe para ser apagado')
    
    if request.method == 'DELETE':
        operation = resource.delete()
        data = {}
        if operation:
            data["sucess"] = "recurso apagado com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "recurso não foi apagado com sucesso"
        return Response(data=data)


#------------------------------------Categoria------------------------------------#
@api_view(['GET'])
def getCategory(request):
    if request.method == 'GET':
        category = Category.objects.all()
        serializer = CategorySerializer(category, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addCategory(request):
    if request.method == 'POST':
        serializer = CategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def deleteCategory(request, slug):
    try:
        category =  Category.objects.filter(name=slug).first()
    except Category.DoesNotExit:
        return Response('A categoria nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = category.delete()
        data = {}
        if operation:
            data["sucess"] = "categoria apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "categoria não foi apagada com sucesso"
        return Response(data=data)



#------------------------------------SubCategoria------------------------------------#

@api_view(['GET'])
def getSubCategory(request):
    if request.method == 'GET':
        subCategory = Subcategory.objects.all()
        serializer = SubCategorySerializer(subCategory, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addSubCategory(request):
    if request.method == 'POST':
        serializer = SubCategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def deleteSubCategory(request, slug):
    try:
        subCategory =  Subcategory.objects.filter(name=slug).first()
    except Category.DoesNotExit:
        return Response('A categoria nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = subCategory.delete()
        data = {}
        if operation:
            data["sucess"] = "categoria apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "categoria não foi apagada com sucesso"
        return Response(data=data)

#------------------------------------Categoria_de_um_Produto------------------------------------#

@api_view(['GET'])
def getProductCategory(request):
    if request.method == 'GET':
        productCategory = ProductCategory.objects.all()
        serializer = ProductCategorySerializer(productCategory, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addProductCategory(request):
    if request.method == 'POST':
        serializer = ProductCategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def deleteProductCategory(request, slug):
    try:
        productCategory =  ProductCategory.objects.filter(product=slug).first()
    except ProductCategory.DoesNotExit:
        return Response('A categoria do produto nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = productCategory.delete()
        data = {}
        if operation:
            data["sucess"] = "categoria do produto apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "categoria do produto não foi apagada com sucesso"
        return Response(data=data)


#------------------------------------SubCategoria_de_um_Produto------------------------------------#

@api_view(['GET'])
def getProductSubCategory(request):
    if request.method == 'GET':
        productCategory = ProductSubcategory.objects.all()
        serializer = ProductCategorySerializer(productCategory, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addProductSubCategory(request):
    if request.method == 'POST':
        serializer = ProductCategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def deleteProductSubCategory(request, slug):
    try:
        productSubCategory =  ProductSubcategory.objects.filter(product=slug).first()
    except ProductSubcategory.DoesNotExit:
        return Response('A Subcategoria do produto nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = productSubCategory.delete()
        data = {}
        if operation:
            data["sucess"] = "Subcategoria do produto apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "Subcategoria do produtonão foi apagada com sucesso"
        return Response(data=data)


#------------------------------------TransporterBase------------------------------------#

@api_view(['GET'])
def getTransporterBase(request):
    if request.method == 'GET':
        transporterBase = TransporterBase.objects.all()
        serializer = TransporterBaseSerializer(transporterBase, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addTransporterBase(request):
    if request.method == 'POST':
        serializer = TransporterBaseSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def deleteTransporterBase(request, slug):
    try:
        transporterBase =  TransporterBase.objects.filter(id=slug).first()
    except TransporterBase.DoesNotExit:
        return Response('A base do transportador nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = transporterBase.delete()
        data = {}
        if operation:
            data["sucess"] = "base do transportador apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "base do transportador foi apagada com sucesso"
        return Response(data=data)

#------------------------------------Fuel------------------------------------#

@api_view(['GET'])
def getFuel(request):
    if request.method == 'GET':
        fuel = Fuel.objects.all()
        serializer = FuelSerializer(fuel, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addFuel(request):
    if request.method == 'POST':
        serializer = FuelSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def deleteFuel(request, slug):
    try:
        fuel =  Fuel.objects.filter(name=slug).first()
    except Fuel.DoesNotExit:
        return Response('A base do transportador nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = fuel.delete()
        data = {}
        if operation:
            data["sucess"] = "base do transportador apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "base do transportador foi apagada com sucesso"
        return Response(data=data)


#------------------------------------Vehicle------------------------------------#
@api_view(['GET'])
def getVehicle(request):
    if request.method == 'GET':
        vehicle = Vehicle.objects.all()
        serializer = ProductCategorySerializer(vehicle, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addVehicle(request):
    if request.method == 'POST':
        serializer = VehicleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def deleteVehicle(request, slug):
    try:
        vehicle =  Vehicle.objects.filter(registration=slug).first()
    except Vehicle.DoesNotExit:
        return Response('A base do transportador nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = vehicle.delete()
        data = {}
        if operation:
            data["sucess"] = "base do transportador apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "base do transportador foi apagada com sucesso"
        return Response(data=data)


#------------------------------------Transport------------------------------------#

@api_view(['GET'])
def getTransport(request):
    if request.method == 'GET':
        transport = Transport.objects.all()
        serializer = TransportSerializer(transport, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addTransport(request):
    if request.method == 'POST':
        serializer = TransportSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def deleteTransport(request, slug):
    try:
        transport =  Transport.objects.filter(id=slug).first()
    except Transport.DoesNotExit:
        return Response('A base do transportador nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = transport.delete()
        data = {}
        if operation:
            data["sucess"] = "base do transportador apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "base do transportador foi apagada com sucesso"
        return Response(data=data)

#------------------------------------pollutionStorage------------------------------------#


@api_view(['GET'])
def getPollutionStorage(request):
    if request.method == 'GET':
        pollutionStorage = PollutionStorage.objects.all()
        serializer = PollutionStorageSerializer(pollutionStorage, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addPollutionStorage(request):
    if request.method == 'POST':
        serializer = PollutionStorageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def deletePollutionStorage(request, slug):
    try:
        pollutionStorage =  PollutionStorage.objects.filter(id=slug).first()
    except PollutionStorage.DoesNotExit:
        return Response('A base do transportador nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = pollutionStorage.delete()
        data = {}
        if operation:
            data["sucess"] = "base do transportador apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "base do transportador foi apagada com sucesso"
        return Response(data=data)


#------------------------------------pollutionProduct------------------------------------#
@api_view(['GET'])
def getPollutionProduct(request):
    if request.method == 'GET':
        pollutionProduct = PollutionProduct.objects.all()
        serializer = PollutionProductSerializer(pollutionProduct, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addPollutionProduct(request):
    if request.method == 'POST':
        serializer = PollutionProductSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def deletePollutionProduct(request, slug):
    try:
        pollutionProduct =  PollutionProduct.objects.filter(id=slug).first()
    except PollutionProduct.DoesNotExit:
        return Response('A base do transportador nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = pollutionProduct.delete()
        data = {}
        if operation:
            data["sucess"] = "base do transportador apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "base do transportador foi apagada com sucesso"
        return Response(data=data)


#------------------------------------pollutionTransport------------------------------------#
@api_view(['GET'])
def getPollutionTransport(request):
    if request.method == 'GET':
        pollutionTransport = PollutionTransport.objects.all()
        serializer = PollutionTransportSerializer(pollutionTransport, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addPollutionTransport(request):
    if request.method == 'POST':
        serializer = PollutionTransportSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def deletePollutionTransport(request, slug):
    try:
        pollutionTransport =  PollutionTransport.objects.filter(id=slug).first()
    except PollutionTransport.DoesNotExit:
        return Response('A base do transportador nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = pollutionTransport.delete()
        data = {}
        if operation:
            data["sucess"] = "base do transportador apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "base do transportador foi apagada com sucesso"
        return Response(data=data)
#------------------------------------resourceStorage------------------------------------#

@api_view(['GET'])
def getResourceStorage(request):
    if request.method == 'GET':
        resourceStorage = ResourceStorage.objects.all()
        serializer = ResourceStorageSerializer(resourceStorage, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addResourceStorage(request):
    if request.method == 'POST':
        serializer = ResourceStorageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def deleteResourceStorage(request, slug):
    try:
        resourceStorage =  ResourceStorage.objects.filter(id=slug).first()
    except ResourceStorage.DoesNotExit:
        return Response('A base do transportador nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = resourceStorage.delete()
        data = {}
        if operation:
            data["sucess"] = "base do transportador apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "base do transportador foi apagada com sucesso"
        return Response(data=data)


#------------------------------------resourceProduct------------------------------------#
@api_view(['GET'])
def getResourceProduct(request):
    if request.method == 'GET':
        resourceProduct = ResourceProduct.objects.all()
        serializer = ResourceProductSerializer(resourceProduct, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addResourceProduct(request):
    if request.method == 'POST':
        serializer = ResourceProductSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def deleteResourceProduct(request, slug):
    try:
        resourceProduct =  ResourceProduct.objects.filter(id=slug).first()
    except ResourceProduct.DoesNotExit:
        return Response('A base do transportador nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = resourceProduct.delete()
        data = {}
        if operation:
            data["sucess"] = "base do transportador apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "base do transportador foi apagada com sucesso"
        return Response(data=data)



#------------------------------------resourceTransport------------------------------------#
@api_view(['GET'])
def getResourceTransport(request):
    if request.method == 'GET':
        resourceTransport = ResourceTransport.objects.all()
        serializer = ResourceTransportSerializer(resourceTransport, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addResourceTransport(request):
    if request.method == 'POST':
        serializer = ResourceTransportSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def deleteResourceTransport(request, slug):
    try:
        resourceTransport =  ResourceTransport.objects.filter(id=slug).first()
    except ResourceTransport.DoesNotExit:
        return Response('A base do transportador nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = resourceTransport.delete()
        data = {}
        if operation:
            data["sucess"] = "base do transportador apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "base do transportador foi apagada com sucesso"
        return Response(data=data)

#------------------------------------orderItem------------------------------------#
@api_view(['GET'])
def getOrderItem(request):
    if request.method == 'GET':
        orderItem = OrderItem.objects.all()
        serializer = OrderItemSerializer(orderItem, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addOrderItem(request):
    if request.method == 'POST':
        serializer = OrderItemSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def deleteOrderItem(request, slug):
    try:
        orderItem =  OrderItem.objects.filter(id=slug).first()
    except OrderItem.DoesNotExit:
        return Response('A base do transportador nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = orderItem.delete()
        data = {}
        if operation:
            data["sucess"] = "base do transportador apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "base do transportador foi apagada com sucesso"
        return Response(data=data)

#------------------------------------order------------------------------------#
@api_view(['GET'])
def getOrder(request):
    if request.method == 'GET':
        order = Order.objects.all()
        serializer = OrderSerializer(order, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addOrder(request):
    if request.method == 'POST':
        serializer = OrderSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def deleteOrder(request, slug):
    try:
        order =  Order.objects.filter(id=slug).first()
    except Order.DoesNotExit:
        return Response('A base do transportador nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = order.delete()
        data = {}
        if operation:
            data["sucess"] = "base do transportador apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "base do transportador foi apagada com sucesso"
        return Response(data=data)


#------------------------------------orderHistory------------------------------------#

@api_view(['GET'])
def getOrderHistory(request):
    if request.method == 'GET':
        orderHistory = OrderHistory.objects.all()
        serializer = OrderHistorySerializer(orderHistory, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def addOrderHistory(request):
    if request.method == 'POST':
        serializer = OrderHistorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['DELETE'])
def deleteOrderHistory(request, slug):
    try:
        orderHistory =  OrderHistory.objects.filter(id=slug).first()
    except OrderHistory.DoesNotExit:
        return Response('A base do transportador nao existe para ser apagada')
    
    if request.method == 'DELETE':
        operation = orderHistory.delete()
        data = {}
        if operation:
            data["sucess"] = "base do transportador apagada com sucesso"
            return Response(data=data)
        else:
             data["failure"] = "base do transportador foi apagada com sucesso"
        return Response(data=data)
