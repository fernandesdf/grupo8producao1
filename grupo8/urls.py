"""grupo8 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from main_app import views as main_views
from api import views as api_view


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main_app.urls')),
    path('register/', main_views.register, name='register'),
    path('help/', main_views.help, name='help'),
    path('profile/', main_views.profile, name='profile'),
    path('profile/delete', main_views.deleteUser, name="UserDelete"),
    path('login/', main_views.login, name='login'),
    path("accounts/", include("allauth.urls")),
    path('logout/', auth_views.LogoutView.as_view(template_name='main_app/logout.html'), name='logout'),


]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    #REST API urls
"""     path('api/', include('api.urls')),
    path('api/', api_view.getProducts, name='api'),
    path('api/add_product/', api_view.addProduct,name='add_product'),
    path('api/pollution/', api_view.getPollution,name='poluicao'),
    path('api/pollution/addpollution', api_view.addPollution,name='addpoluicao'),
    path('api/pollution/delpollution/<slug>', api_view.DeletePollution,name='deletepoluicao'),
    path('api/resource/', api_view.getResources,name='resources'),
    path('api/resource/addresource', api_view.addResource,name='addresource'),
    path('api/resource/deleteresource/<slug>', api_view.DeleteResource,name='deleteresource'),
    path('api/users/type', api_view.getUserType,name='users-type'),
    path('api/users/', api_view.getCustomUser,name='users'),
    path("accounts/", include("allauth.urls")), """