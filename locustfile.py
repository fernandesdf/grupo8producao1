from json import loads
from random import randint
from locust import HttpUser, task

class HelloWorldUser(HttpUser):
    @task
    def on_start(self):
        global api_token
        global users_list
        global products_list
        global storages_list
        
        # set token URL
        api_token_url = "api-token-auth/"

        # get API token with POST method
        api_token = self.client.post(api_token_url, {'username': "admin",
        'password': "123"}).json()['token']
        

        # get users list
        users_list = loads(self.client.get("api/CustomUser/", headers = {
        'Authorization': "Token " + api_token}).content) 

        #get products list
        products_list = loads(self.client.get("api/Product/", headers = {
        'Authorization': "Token " + api_token}).content)

        #get storages list
        storages_list = loads(self.client.get("api/Storage/", headers = {
        'Authorization': "Token " + api_token}).content)
    

    @task
    def getRandomUser(self):
        global users_list

        picker = randint(0, len(users_list) - 1)

        get_req_url = "api/CustomUser/" + str(users_list[picker]['id'])

        #GET request for random user
        self.client.get(get_req_url, headers = { 'Authorization': "Token " +
        api_token})
    
    @task
    def getRandomProduct(self):
        global products_list

        picker = randint(0, len(products_list) - 1)

        get_req_url = "api/Product/" + str(products_list[picker]['id'])

        #GET request for random user
        self.client.get(get_req_url, headers = { 'Authorization': "Token " +
        api_token})

    @task
    def getRandomStorage(self):
        global storages_list

        picker = randint(0, len(storages_list) - 1)

        get_req_url = "api/Storage/" + str(storages_list[picker]['id'])

        #GET request for random user
        self.client.get(get_req_url, headers = { 'Authorization': "Token " +
        api_token}) 
        