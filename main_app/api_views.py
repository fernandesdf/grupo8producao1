from rest_framework import viewsets
from rest_framework.permissions import BasePermission,IsAuthenticated, SAFE_METHODS, IsAdminUser
from rest_framework import permissions


from .models import *
from .serializers import *
from django_filters.rest_framework import DjangoFilterBackend




class UserTypeViewSet(viewsets.ModelViewSet):

    permission_classes = [IsAdminUser]
    queryset = UserType.objects.all()
    serializer_class = UserTypeSerializer
    

class CustomUserViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserserializer
    http_method_names = ["get", "head", "patch", "put", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['user_type']




class ProfileViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    http_method_names = ["get", "head", "patch", "put", "delete"]

class StorageViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = Storage.objects.all()
    serializer_class = StorageSerializer
    http_method_names = ["get", "head", "patch", "put", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['provider']



class ProductViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    http_method_names = ["get", "head", "patch", "put", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['storage', 'id']


class PollutionViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = Pollution.objects.all()
    serializer_class = PollutionSerializer
    http_method_names = ["get", "head", "patch", "put", "delete"]


class ResourceViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = Resource.objects.all()
    serializer_class = ResourceSerializer
    http_method_names = ["get", "head", "patch", "put", "delete"]


class CategoryViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    http_method_names = ["get", "head", "patch", "put", "delete"]


class SubCategoryViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = Subcategory.objects.all()
    serializer_class = SubCategorySerializer
    http_method_names = ["get", "head", "patch", "put", "delete"]

class ProductCategoryViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = ProductCategory.objects.all()
    serializer_class = ProductCategorySerializer
    http_method_names = ["get", "head", "patch", "put", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['product', 'category']


class ProductSubCategoryViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = ProductSubcategory.objects.all()
    serializer_class = ProductSubcategorySerializer
    http_method_names = ["get", "head", "patch", "put", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['product', 'subcategory']


class TransporterBaseViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = TransporterBase.objects.all()
    serializer_class = TransporterBaseSerializer
    http_method_names = ["get", "head", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['transporter']



class FuelViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = Fuel.objects.all()
    serializer_class = FuelSerializer
    http_method_names = ["get", "head", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['pollution_type']
    


class VehicleViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer
    http_method_names = ["get", "head", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['base', 'registration', 'brand', 'model', 'fuel']


class TransportViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = Transport.objects.all()
    serializer_class = TransportSerializer
    http_method_names = ["get", "head", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['transporter', 'vehicle']



###viewsets da app Chat

class PollutionStorageViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = PollutionStorage.objects.all()
    serializer_class = PollutionStorageSerializer
    http_method_names = ["get", "head", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['pollution', 'storage']

class PollutionProductViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = PollutionProduct.objects.all()
    serializer_class = PollutionProductSerializer
    http_method_names = ["get", "head", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['pollution', 'product']

class PollutionTransportViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = PollutionTransport.objects.all()
    serializer_class = PollutionTransportSerializer
    http_method_names = ["get", "head", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['pollution', 'transport']

class ResourceStorageViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = ResourceStorage.objects.all()
    serializer_class = ResourceStorageSerializer
    http_method_names = ["get", "head", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['storage', 'resource']

class ResourceProductViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = ResourceProduct.objects.all()
    serializer_class = ResourceProductSerializer
    http_method_names = ["get", "head", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['product', 'resource']


class ResourceTransportViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = ResourceTransport.objects.all()
    serializer_class = ResourceTransportSerializer
    http_method_names = ["get", "head", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['transport', 'resource']




class OrderItemViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer
    http_method_names = ["get", "head", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['user', 'complete', 'item']


class OrderViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    http_method_names = ["get", "head", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['user', 'complete']


class OrderHistoryViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = OrderHistory.objects.all()
    serializer_class = OrderHistorySerializer
    http_method_names = ["get", "head", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['customer', 'order']
