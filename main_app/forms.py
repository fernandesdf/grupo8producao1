from pyexpat import model
from django import forms
#from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import *
from django.utils.translation import gettext_lazy as _
from django_countries.fields import CountryField
#_____________________________________________users______________
class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = CustomUser
        fields = ['username', 'email', 'user_type', 'password1', 'password2']



class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = CustomUser
        fields = ['username', 'first_name', 'last_name', 'email']

class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields=['image','address', 'cell_phone', 'nif']

class UserDeleteForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = []
        
"""
class ProviderUpdateForm(forms.ModelForm):
    class Meta:
        model = Provider
        fields=['storage']#,'storage_date_register'
"""
class TransporterUpdateForm(forms.ModelForm):
    class Meta:
        model = Transporter
        fields=['company']#, 'company_date_register'

#_____________________________________________main_app______________
class StorageRegisterForm(forms.ModelForm):

    class Meta:
        model = Storage
        fields = ['address']

class StorageUpdateForm(forms.ModelForm):

    class Meta:
        #especificar o model
        model = Storage
        fields = ['address']

class StorageDeleteForm(forms.ModelForm):
    class Meta:
        model = Storage
        fields = []

class ProductRegisterForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['title', 'price', 'description', 'image', 'in_stock']

class ProductUpdateForm(forms.ModelForm):
    class Meta:
        model = Product
        fields=['title', 'price', 'description', 'image', 'in_stock']

class ProductRegisterResourceForm(forms.ModelForm):
    class Meta:
        model = ResourceProduct
        fields = ['resource', 'consumed']

class ProductDeleteForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = []

class BaseRegisterForm(forms.ModelForm):

    class Meta:
        model = TransporterBase
        fields = ['address']

class BaseDetailForm(forms.ModelForm):

    class Meta:
        model = Vehicle
        fields = [ 'brand', 'model']

class BaseDeleteForm(forms.ModelForm):

    class Meta:
        model = TransporterBase
        fields = []



class TransporterBaseUpdateForm(forms.ModelForm):
    class Meta:
        model = TransporterBase
        fields=['address']

class VehicleRegisterForm(forms.ModelForm):

    class Meta:
        model = Vehicle
        fields = [ 'brand', 'registration', 'model', 'fuel']

class VehicleUpdateForm(forms.ModelForm):
    class Meta:
        model = Vehicle
        fields=['brand', 'registration', 'model', 'fuel']    

class VehicleDeleteForm(forms.ModelForm):
    class Meta:
        model = Vehicle
        fields = []
        

PAYMENT_CHOICES = (
    ('P', 'Paypal'),
    ('C', 'Cartao de Débito/Crédito')
)
class CheckoutForm(forms.Form):
    nome = forms.CharField(required=True)
    apelido = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    morada = forms.CharField(required=True)
    codigo_postal = forms.CharField(required=True)
    nif = forms.CharField(required=False)
    pais = CountryField(blank_label='(Seleciona o teu país)').formfield(required=True)
    metodo_de_pagamento = forms.ChoiceField(widget=forms.RadioSelect, choices=PAYMENT_CHOICES,required=True)
            

