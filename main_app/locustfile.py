from locust import HttpUser, TaskSet, task,between
import time


USER_CREDENTIALS = [
    ("testProvider", "newtesting123"),
    ("testConsumer", "newtesting123"),
    ("admin", "123"),
    ("admin", "444"),
]


USER_REGISTER = [
    ('quim', 'quim@gmail.com', 'Consumidor', 'newtesting123', 'newtesting123'),
    ('joao', 'joao@gmail.com', 'Consumidor', 'newtesting123', 'newtesting123'),
    ('teste', 'teste@gmail.com', 'Fornecedor', 'newtesting123', 'newtesting123'),
    ('porto', 'porto@gmail.com', 'Fornecedor', 'newtesting123', 'newtesting123'),
    ('benfica', 'benfica@gmail.com', 'Transportador', 'newtesting', 'newtesting'),
    ('matilde', 'matilde@gmail.com', 'Transportador', 'newtesting', 'newtesting'),
]

class UserActions(TaskSet):
    wait_time = between(3, 5)
    

    def on_start(self):
        """ on_start is called when a Locust start before any task is scheduled """
        self.fazer_login()

    @task
    def fazer_login(self):
        """user faz login"""
        response = self.client.get('login/')
        if len(USER_CREDENTIALS) > 0:
            user, passw = USER_CREDENTIALS.pop()
            csrftoken = response.cookies['csrftoken']
            self.client.post('login/',
                         {'username': user, 'password': passw},
                         headers={'X-CSRFToken': csrftoken})
            #self.client.post("login", {'username': user, 'password': passw})

    @task(1)
    def index_page(self):
        self.client.get("")

    @task
    def profiles(self):
        self.client.get("profile/")

    @task(1)
    def product(self):
        self.client.get("product/1")

    @task(1)
    def product2(self):
        self.client.get("product/2")

    @task(1)
    def login(self):
        self.client.get("login/")
    
    @task
    def carrinho(self):
        self.client.get("cart/")

    @task(1)
    def registar(self):
        self.client.get("register/")

    @task
    def registar_client(self):
        response = self.client.get('register/')
        if len(USER_REGISTER) > 0:
            user, email, tipo, passw1, passw2 = USER_REGISTER.pop()
            csrftoken = response.cookies['csrftoken']
            self.client.post('register/',
                         {'username': user, 'email': email, 'tipo':tipo, 'password1':passw1, 'password2':passw2},
                         headers={'X-CSRFToken': csrftoken})
    @task
    def logout(self):
        self.client.get("logout/")

    def on_stop(self):
        self.logout()
        
class User(HttpUser):
    tasks = [UserActions]
    min_wait = 5000
    max_wait = 60000
    host="http://127.0.0.1:8000/"