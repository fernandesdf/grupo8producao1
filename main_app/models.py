import email
from email.policy import default
from tabnanny import verbose
from django.db import models
from django.contrib.auth.models import AbstractUser
from PIL import Image
from django.utils.translation import gettext_lazy as _
from django.shortcuts import reverse

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from rest_framework.authtoken.models import Token

    
@receiver(post_save, sender = settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance = None, created = False, **kwargs):
    if created:
        Token.objects.create(user=instance)

#_____________________________________________users________________
class UserType(models.Model):
    description = models.CharField(verbose_name=_("Description"), blank=True, null=True, max_length=20)

    def __str__(self):
        return self.description

class CustomUser(AbstractUser):
    user_type = models.ForeignKey(UserType, verbose_name=_("User type"), on_delete=models.CASCADE, null=True) #Consumer, Transporter, Provider



class Profile(models.Model):
    user = models.OneToOneField(CustomUser, verbose_name=_("User"), on_delete=models.CASCADE)
    image = models.ImageField(verbose_name=_("Image"), default='default.png', upload_to='profile_pics')
    # Isto tem que ser mudado pq as moradas tem que ter coordenadas, disse o prof
    address = models.CharField(verbose_name=_("Address"), blank=True, max_length=60)
    cell_phone = models.CharField(verbose_name=_("Phone number"), blank=True, max_length=9)
    nif = models.CharField(verbose_name=_("NIF"), blank=True, max_length=9)
    
    def __str__(self):
        return f'{self.user.username} Profile'

    def save(self, *args, **kwargs):
        super(Profile, self).save(*args, **kwargs)

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size) # resize
            img.save(self.image.path) # override the img with the new size

class Provider(models.Model):
    user = models.OneToOneField(CustomUser, verbose_name=_("User"), on_delete=models.CASCADE)
    #storage_date_register = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return f'{self.user.username}'

class Consumer(models.Model):
    user = models.OneToOneField(CustomUser, verbose_name=_("User"), on_delete=models.CASCADE)
    def __str__(self):
        return f'{self.user.username}'


class Transporter(models.Model):
    user = models.OneToOneField(CustomUser, verbose_name=_("User"), on_delete=models.CASCADE)
    company = models.CharField(verbose_name=_("Company"), blank=True, null=True, max_length=50)
    #company_date_register = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return f'{self.user.username}'

#_____________________________________________main_app_____________
# Storage

class Storage(models.Model):
    provider = models.ForeignKey(CustomUser, verbose_name=_("Provider"), on_delete=models.CASCADE)
    # Isto tem que ser mudado pq as moradas tem que ter coordenadas, disse o prof
    address = models.CharField(verbose_name=_("Address"), max_length=60, blank=True, null=True)
    def __str__(self):
        return f'Armazém: {self.address}'

#_______________________________""""_______________________________
# Product

class Product(models.Model):
    title = models.CharField(verbose_name=_("Product's title"), max_length=100,blank=True, null=True)
    price = models.FloatField(verbose_name=_("Price"), blank=True, null=True)
    discount_price = models.FloatField(verbose_name=_("Discount price"), blank=True, null=True)
    description = models.TextField(verbose_name=_("Description"), blank=True, null=True)
    image = models.ImageField(verbose_name=_("Image"), default='default-product.jpg', upload_to='products_pics')
    storage = models.ForeignKey(Storage, verbose_name=_("Storage"), on_delete = models.CASCADE)
    in_stock = models.IntegerField(verbose_name=_("In stock"), blank=True, null=True)
    slug = models.SlugField()

    def __str__(self):
        return f'{self.title}'

    def save(self, *args, **kwargs):
        super(Product, self).save(*args, **kwargs)

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size) # resize
            img.save(self.image.path) # override the img with the new size

    def get_absolute_url(self):
        return reverse("product_detail", kwargs={'slug': self.slug})

    def get_add_cart_url(self):
        return reverse("add-to-cart", kwargs={'slug': self.slug}) 

    def get_remove_from_cart_url(self):
        return reverse("remove-from-cart", kwargs={'slug': self.slug}) 

class Category(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=30,blank=True, null=True)

    def __str__(self):
        return f'{self.name}'

class Subcategory(models.Model):
    category = models.ForeignKey(Category, verbose_name=_("Category"), on_delete=models.CASCADE)
    name = models.CharField(verbose_name=_("Name"), max_length=30,blank=True, null=True)

    def __str__(self):
        return f'{self.name}'

class ProductCategory(models.Model):
    product = models.ForeignKey(Product, verbose_name=_("Product"), on_delete=models.CASCADE)
    category = models.ForeignKey(Category, verbose_name=_("Category"), on_delete=models.CASCADE)

    def __str__(self):
        return f'Produto: {self.product}, Categoria: {self.category}'

class ProductSubcategory(models.Model):
    product = models.ForeignKey(Product, verbose_name=_("Product"), on_delete=models.CASCADE)
    subcategory = models.ForeignKey(Subcategory, verbose_name=_("Subcategory"), on_delete=models.CASCADE)

    def __str__(self):
        return f'Produto: {self.product}, Subcategoria: {self.subcategory}'

#_______________________________""""_______________________________
# Pollution

class Pollution(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=60, blank=True, null=True)
    unit = models.CharField(verbose_name=_("Unit"), max_length=10, blank=True, null=True)
    def __str__(self):
        return f'{self.name}'

#_______________________________""""_______________________________
# Resource

class Resource(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=60, blank=True, null=True)
    unit = models.CharField(verbose_name=_("Unit"), max_length=10, blank=True, null=True)
    def __str__(self):
        return f'{self.name}'

#_______________________________""""_______________________________
# Transport

class TransporterBase(models.Model):
    # Isto tem que ser mudado pq as moradas tem que ter coordenadas, disse o prof
    transporter = models.ForeignKey(CustomUser, verbose_name=_("Transporter"), on_delete=models.CASCADE)
    address = models.CharField(verbose_name=_("Address"), max_length=60, blank=True, null=True)

    def __str__(self):
        return f'{self.address}'

class Fuel(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=30,blank=True, null=True)
    # pollution_type -> eh necessario este atributo?
    pollution_type = models.ForeignKey(Pollution, verbose_name=_("Pollution"), on_delete=models.CASCADE)
    pollution_per_km = models.FloatField(verbose_name=_("Pollution generated per Km"), blank=True, null=True)
    
    def __str__(self):
        return f'{self.name}'

class Vehicle(models.Model):
    base = models.ForeignKey(TransporterBase, verbose_name=_("Transporter's base"), on_delete=models.CASCADE)
    registration = models.CharField(verbose_name=_("Registration"), max_length=6, blank=True, null=True) 
    brand = models.CharField(verbose_name=_("Brand"), max_length=20, blank=True, null=True) 
    model = models.CharField(verbose_name=_("Model"), max_length=20, blank=True, null=True) 
    fuel = models.ForeignKey(Fuel, verbose_name=_("Fuel"), on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.brand} {self.model}, {self.registration}'

class Transport(models.Model):
    transporter = models.ForeignKey(CustomUser, verbose_name=_("Transporter"), on_delete=models.CASCADE)
    start = models.CharField(verbose_name=_("Departure"), max_length=60, blank=True, null=True) 
    end = models.CharField(verbose_name=_("Arrival"), max_length=60, blank=True, null=True) 
    vehicle = models.ForeignKey(Vehicle, verbose_name=_("Vehicle"), on_delete=models.CASCADE)
    #distanceTraveled (kms) -> se tiver o start e o end com coordenadas gps, n deve ser
    #                           preciso a distancia, visto q deve ser possivel calc
    #                           atraves das coordenas

    def __str__(self):
        return f'Partida: {self.start}, Chegada: {self.end}'

#_______________________________""""_______________________________
# Pollution related

class PollutionStorage(models.Model):
    storage = models.ForeignKey(Storage, verbose_name=_("Storage"), on_delete=models.CASCADE)
    pollution = models.ForeignKey(Pollution, verbose_name=_("Pollution"), on_delete=models.CASCADE)
    generated = models.FloatField(verbose_name=_("Pollution generated"), blank=True, null=True)
    def __str__(self):
        return f'Poluição gerada por {self.storage}: {self.generated}'

class PollutionProduct(models.Model):
    product = models.ForeignKey(Product, verbose_name=_("Product"), on_delete=models.CASCADE)
    pollution = models.ForeignKey(Pollution, verbose_name=_("Pollution"), on_delete=models.CASCADE)
    generated = models.FloatField(verbose_name=_("Pollution generated"), blank=True, null=True)
    def __str__(self):
        return f'Poluição gerada por {self.product}: {self.generated}'

class PollutionTransport(models.Model):
    transport = models.ForeignKey(Transport, verbose_name=_("Transport"), on_delete=models.CASCADE)
    pollution = models.ForeignKey(Pollution, verbose_name=_("Pollution"), on_delete=models.CASCADE)
    generated = models.FloatField(verbose_name=_("Pollution generated"), blank=True, null=True)
    def __str__(self):
        return f'Poluição gerada por {self.transport}: {self.generated}'

#_______________________________""""_______________________________
# Resource related

class ResourceStorage(models.Model):
    storage = models.ForeignKey(Storage, verbose_name=_("Storage"), on_delete=models.CASCADE)
    resource = models.ForeignKey(Resource, verbose_name=_("Resource"), on_delete=models.CASCADE)
    consumed = models.FloatField(verbose_name=_("Consumed"), blank=True, null=True)
    def __str__(self):
        return f'Armazém: {self.storage}, Recurso: {self.resource}'

class ResourceProduct(models.Model):
    product = models.ForeignKey(Product, verbose_name=_("Product"), on_delete=models.CASCADE)
    resource = models.ForeignKey(Resource, verbose_name=_("Resource"), on_delete=models.CASCADE)
    consumed = models.FloatField(verbose_name=_("Consumed"), blank=True, null=True)
    def __str__(self):
        return f'Produto: {self.product}, Recurso: {self.resource}'

class ResourceTransport(models.Model):
    transport = models.ForeignKey(Transport, verbose_name=_("Transport"), on_delete=models.CASCADE)
    resource = models.ForeignKey(Resource, verbose_name=_("Resource"), on_delete=models.CASCADE)
    consumed = models.FloatField(verbose_name=_("Consumed"), blank=True, null=True)
    def __str__(self):
        return f'Transporte: {self.transport}, Recurso: {self.resource}'

#_______________________________""""_______________________________
# Order
class OrderItem(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, verbose_name=_("User"), blank=True, null=True)
    complete = models.BooleanField(default=False)
    item = models.ForeignKey(Product, verbose_name=_("Product"), on_delete=models.CASCADE)
    amount = models.IntegerField(verbose_name=_("Amount"), default=1)
    session_id = models.CharField(max_length=100)
   
    def get_total(self):
        total = self.item.price * self.amount
        return total

    def get_discount_total(self):
        total = self.item.discount_price * self.amount
        return total

    def get_amount_saved(self):
        return self.get_total() - self.get_discount_total()
    
    def get_final_price(self):
        if self.item.discount_price:
            return self.get_discount_total()
        return self.get_total()

    def __str__(self):
        return f'{self.item}'

class Order(models.Model):#              on_delete=models.SET_NULL -> poe a null este atributo quando o user eh apagado
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, verbose_name=_("User"), blank=True, null=True)
    items = models.ManyToManyField(OrderItem)
    date_ordered = models.DateTimeField(auto_now_add=True)
    complete = models.BooleanField(default=False)
    transaction_id = models.CharField(max_length=200, null=True)
    session_id = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.user}'

    @property # pode ser chamado cm se fosse um atributo do modelo
    def get_cart_total(self):
        total = 0
        for order_item in self.items.all():
            total += order_item.get_final_price()
        
        return total


class OrderHistory(models.Model):
    customer = models.ForeignKey(CustomUser, verbose_name=_("Consumer"), on_delete=models.CASCADE)
    order = models.OneToOneField(Order, verbose_name=_("Order"), on_delete=models.CASCADE)
    
    def __str__(self):
        return f'Encomenda: {self.order}, Consumidor: {self.customer}'

