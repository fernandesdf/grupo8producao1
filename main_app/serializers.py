from rest_framework import serializers
from .models import *


#_______________________________""""_______________________________
# Users

class UserTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserType
        fields= "__all__"


class CustomUserserializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields= ['id','password','last_login','is_superuser','username','first_name','last_name','email','is_staff','is_active','date_joined','user_type','groups','user_permissions']


class ProviderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Provider
        fields= ['id','user']


class ConsumerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Consumer
        fields= ['id','user']


class TransporterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transporter
        fields= ['id','user','company']



#_______________________________""""_______________________________
# Profile
class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields= ['id','user', 'image', 'address', 'cell_phone', 'nif']


#_______________________________""""_______________________________
# Storage

class StorageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Storage
        fields= ['id','provider', 'address']



#_______________________________""""_______________________________
# Product
class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model= Product
        fields =['id','title', 'price', 'discount_price', 'description', 'image','storage', 'in_stock'   ]

#_______________________________""""_______________________________
# Category

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields= ['id','name']

#_______________________________""""_______________________________
# SubCategory

class SubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Subcategory
        fields= ['id','category', 'name']

#_______________________________""""_______________________________
# ProductCategory

class ProductCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCategory
        fields= ['id','product', 'category']

#_______________________________""""_______________________________
# SubCategory

class  ProductSubcategorySerializer(serializers.ModelSerializer):
    class Meta:
        model =  ProductSubcategory
        fields= ['id','product', 'subcategory']



#_______________________________""""_______________________________
# Pollution

class PollutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pollution
        fields = ['id','name', 'unit']


#_______________________________""""_______________________________
# Resource

class ResourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resource
        fields = ['id','name', 'unit']  


#_______________________________""""_______________________________
# Transport

class TransporterBaseSerializer(serializers.ModelSerializer):
    class Meta:
        model =TransporterBase
        fields = ['id','transporter', 'address']

class FuelSerializer(serializers.ModelSerializer):
    class Meta:
        model =Fuel
        fields = ['id','name', 'pollution_type','pollution_per_km']

class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model =Vehicle
        fields = ['id','base', 'registration','brand','model','fuel']


class TransportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transport
        fields = ['id','transporter', 'start', 'end', 'vehicle']


#_______________________________""""_______________________________
# Pollution related

class PollutionStorageSerializer(serializers.ModelSerializer):
    class Meta:
        model = PollutionStorage
        fields = ['id','storage', 'pollution', 'generated']


class PollutionProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = PollutionProduct
        fields = ['id','product', 'pollution', 'generated']


class PollutionTransportSerializer(serializers.ModelSerializer):
    class Meta:
        model = PollutionTransport
        fields = ['id','transport', 'pollution', 'generated']


#_______________________________""""_______________________________
# Resource related


class ResourceStorageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResourceStorage
        fields = ['id','storage', 'resource', 'consumed']



class ResourceProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResourceProduct
        fields = ['id','product', 'resource', 'consumed']



class ResourceTransportSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResourceTransport
        fields = ['id','transport', 'resource', 'consumed']



#_______________________________""""_______________________________
# Order

class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = ['id','user', 'complete', 'item','amount']

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ['id','user', 'items', 'date_ordered','complete','transaction_id']


class OrderHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderHistory
        fields = ['id','customer', 'order']