from django.test import TestCase
from .models import *


class URLTests(TestCase):

    def test_homepage(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_login(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_resgister(self):
        response = self.client.get('/register/')
        self.assertEqual(response.status_code, 200)
    
    def test_cart(self):
        response = self.client.get('/cart/')
        self.assertEqual(response.status_code, 200)
    
    def test_product(self):
        response = self.client.get('/product/')
        self.assertEqual(response.status_code, 200)
    
    def test_product_details(self):
        response = self.client.get('/product/1/')
        self.assertEqual(response.status_code, 200)

    def test_storage(self):
        response = self.client.get('/storage/')
        self.assertEqual(response.status_code, 200)
    
    def test_logout(self):
        response = self.client.get('/logout/')
        self.assertEqual(response.status_code, 200)
    
    def test_profile(self):
        response = self.client.get('/profile/')
        self.assertNotEqual(response.status_code, 200)
   
   
class ModelosTests(TestCase):

    def verify_pollution(self):
        poluicao = Pollution(name="lixo", unit="0.4 kg")
        poluicao.save()
        self.assertEqual(str(poluicao), "lixo")

    def verify_resource(self):
        recurso = Resource(name="madeira", unit="0.4 kg")
        recurso.save()
        self.assertEqual(str(recurso), "madeira")

    def product(self):
        produto = Product(title="ananas",price=2.0, discount_price=0)
        self.assertEqual(str(produto), "ananas")

    def verify_category(self):
        categoria = Category(name="pilhas")
        self.assertEqual(str(categoria), "pilhas")
    
    def verify_subcategory(self):
        subcategoria = Subcategory(name="recarga")
        self.assertEqual(str(subcategoria), "recarga")

    def verify_product_category(self):
        product_category = ProductCategory(product=Product(title="ananas"), category=Category(name="alimentar"))
        record = ProductCategory(product=Product(title="pilhas"), category=Category(name="alimentar"))
        self.assertEqual(product_category, record)

    def verify_product_subcategory(self):
        product_subcategory = ProductSubcategory(product=Product(title="pilhas"), subcategory=Subcategory(name="recarga"))
        record = ProductSubcategory(product=Product(title="ananas"), subcategory=Subcategory(name="recarga"))
        self.assertEqual(product_subcategory, record)
    
    def verify_fuel(self):
        combustivel = Fuel(name="gasolina", pollution_type=Pollution(name="do ar", unit="cm3"), pollution_per_km ="0.3 cm3")
        self.assertIn("gasolina", str(combustivel))
      