
from email import message
from django.shortcuts import render, get_object_or_404
from django.shortcuts import redirect, render
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.views.generic import DetailView,View
from django.db.models import Q
from main_app.forms import *
from .models import *
import uuid

from django.core.paginator import Paginator

from django.conf import settings

from django.contrib.auth import (
    REDIRECT_FIELD_NAME, get_user_model, login as auth_login,
    logout as auth_logout, update_session_auth_hash,
)
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import (
    AuthenticationForm
)
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site

from django.http import HttpResponseRedirect
from django.shortcuts import resolve_url
from django.template.response import TemplateResponse

#pip3 install django-utils
from django.utils.http import url_has_allowed_host_and_scheme, urlsafe_base64_decode

from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters


from django.shortcuts import get_object_or_404

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response


for user in CustomUser.objects.all():
    Token.objects.get_or_create(user=user)

# Create your views here.
#_____________________________________________main_app______________
def home(request):
    products = Product.objects.all()

    p = Paginator(Product.objects.all(), 9)
    page = request.GET.get('page')
    productsPaginado = p.get_page(page)

    context = { 'products':products, 'productsPaginado': productsPaginado }
    return render(request, 'main_app/home.html', context)

def about(request):
    return render(request, 'main_app/about.html', {'title': 'Acerca'})
    
def help(request):
    return render(request, 'main_app/help.html', {'title': 'Ajuda'})

def legal(request):
    return render(request, 'main_app/legal.html', {'title': 'Termos e Condições'})

#_____________________________________________users______________
def register(request):
    if request.user.is_authenticated:
        return redirect('main_app-home')
        
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            Token.objects.get_or_create(user=user)
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Conta { username } criada com sucesso! Já podes iniciar sessão.')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'main_app/register.html', {'form': form})




class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
        })

@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, template_name='main_app/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=AuthenticationForm,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.POST.get(redirect_field_name,
                                   request.GET.get(redirect_field_name, ''))

    if request.method == "POST":
        form = authentication_form(request, data=request.POST)

        userID = CustomUser.objects.filter(username = request.POST["username"]).first()
        

        #token = get_object_or_404(Token, user=userID.id)
        try:
            
            if form.is_valid():
                token = Token.objects.get(user=userID.id)
            # Ensure the user-originating redirection url is safe.
                if not url_has_allowed_host_and_scheme(redirect_to, allowed_hosts=request.get_host()) and token:
                    redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)
                    
                    # Okay, security check complete. Log the user in.
                    auth_login(request, form.get_user())

                    return HttpResponseRedirect(redirect_to)
        except CustomUser.DoesNotExist:
            messages.success(request, f'A sua conta não é válida!')
            form = authentication_form(request)
        except Token.DoesNotExist:
            messages.success(request, f'A sua conta não é válida!')
            form = authentication_form(request)

    else:
        form = authentication_form(request)

    current_site = get_current_site(request)

    context = {
        'form': form,
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
    }
    if extra_context is not None:
        context.update(extra_context)

    if current_app is not None:
        request.current_app = current_app

    return TemplateResponse(request, template_name, context)
    




@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST, 
                                    request.FILES,
                                    instance=request.user.profile)

        if len(CustomUser.objects.filter(username=request.user.username,user_type=3))==1:
            user_type_form = TransporterUpdateForm(
                                    request.POST,
                                    instance=request.user.transporter)
        else:
            user_type_form = None

        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            if user_type_form is not None:
                user_type_form.save()
            messages.success(request, f'A sua conta foi atualizada!')
            return redirect('profile') # makes a get request
            
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

        if len(CustomUser.objects.filter(username=request.user.username,user_type=3))==1:
            user_type_form =  TransporterUpdateForm(instance=request.user.transporter)
            context = {
                'u_form': u_form,
                'p_form': p_form,
                'user_type_form':user_type_form
            }

        else:
            context = {
                'u_form': u_form,
                'p_form': p_form,
            }

    return render(request, 'main_app/profile.html', context)

@login_required
def deleteUser(request):
    if request.method == 'POST':
        delete_form = UserDeleteForm(request.POST, instance=request.user)
        user = request.user
        user.delete()
        messages.info(request, 'A sua conta foi apagada!')
        return redirect('login')
    else:
        delete_form = UserDeleteForm(instance=request.user)

    context = {
        'delete_form': delete_form
    }

    return render(request, 'main_app/user_delete.html', context)

#__________________________________________products,storage______________
@login_required
def storage_register(request):
    if request.method == 'POST':
        user = request.user
        address  = request.POST.get("address")
        userLogged = CustomUser.objects.get(username=user)

        form1 = StorageRegisterForm(request.POST)
        if form1.is_valid():
            Storage.objects.create(provider=userLogged, address=address)

            messages.success(
                request, f'Armazém criado com sucesso! Já podes adicionar produtos.')
            return redirect('storage')#para a pagina do armazem
    else:
        form1 = StorageRegisterForm()
    return render(request, 'main_app/storage_register.html', {'form1': form1})#para a pagina do armazem

@login_required
def storage(request):
    content = Storage.objects.all().filter(provider=request.user)
    context = {
        'storages': content
    }
    return render(request, 'main_app/storage.html', context)

@login_required
def storage_edit(request, storage_id):

    if request.method == 'POST':

        u_form = StorageUpdateForm(request.POST)
        if u_form.is_valid():
            storage = Storage.objects.get(id = storage_id)
            u_form = StorageUpdateForm(request.POST, instance = storage)
            u_form.save()
            messages.success(request, f'O seu armazém foi atualizado!')
            return redirect('storage')

    else:

        storage = Storage.objects.filter(id = storage_id).first()
        u_form = StorageUpdateForm(instance=storage)


    return render(request, 'main_app/storage_edit.html', {'u_form': u_form, 'storage':storage})


class storage_detail(DetailView):
    model = Storage
    def get_context_data(self, **kwargs):
        storage = self.get_object()
        context = super().get_context_data(**kwargs)
        context["products"] = Product.objects.all().filter(storage=storage.id)

        return context

#class product_detail(DetailView):
#    model = Product
#    template_name = "main_app/product_detail.html"

@login_required
def delete_storage(request, storage_id):
    if request.method == 'POST':
        
        storage = Storage.objects.get(id = storage_id)
        delete_form = StorageDeleteForm(request.POST, instance = storage)
        if delete_form.is_valid():
            storage.delete()
            messages.info(request, 'O armazém foi apagado!')
            return redirect('storage')
    else:
        storage = Storage.objects.filter(id = storage_id).first()

        delete_form = StorageDeleteForm(instance=storage)

    context = {
        'storage': storage
    }

    return render(request, 'main_app/delete_storage.html', context)

def product_detail(request, product_id):
    product = Product.objects.filter(id = product_id).first()
    productPollution = PollutionProduct.objects.filter(product=product)
    productResource = ResourceProduct.objects.filter(product=product)

    context = {
        'product': product,
        'pollution': productPollution,
        'resource':productResource

    }

    return render(request, 'main_app/product_detail.html', context)

@login_required
def delete_product(request, product_id):
    if request.method == 'POST':
        
        product = Product.objects.get(id = product_id)
        delete_form = ProductDeleteForm(request.POST, instance = product)
        if delete_form.is_valid():
            product.delete()
            messages.info(request, 'O produto foi apagado!')
            storage_id = product.storage.id
            return redirect('storage_detail', pk=storage_id)
    else:
        product = Product.objects.filter(id = product_id).first()

        delete_form = ProductDeleteForm(instance=product)

    context = {
        'delete_form': product
        
    }

    return render(request, 'main_app/delete_product.html', context)

@login_required
def product_register(request, storage_id):
    if request.method == 'POST':
        storage = Storage.objects.get(id = storage_id)
        #pol_type = request.POST.get("pollution")
        #res_type = request.POST.get("resource")
        #pollution = PollutionInfo.objects.get(name=pol_type)
        #resource = ResourceInfo.objects.get(name=res_type)

        form1 = ProductRegisterForm(request.POST, request.FILES)
        formResource = ProductRegisterResourceForm(request.POST)

        if form1.is_valid() and formResource:
            product = form1.save(commit=False)
            productResource = formResource.save(commit=False)
            
            product.storage = storage
            lastProduct = Product.objects.all().last().id +1
            if lastProduct:
                product.slug = "product-"+str(lastProduct)
            else:
                product.slug = "product-"+str(1)
            product.save()
            productResource.product = product
            productResource.save()
            messages.success(request, f'Produto criado com sucesso!')
            return redirect('storage_detail', pk=storage_id)#para a pagina do armazem
    else:
        form1 = ProductRegisterForm()
        formResource = ProductRegisterResourceForm()

    storage = Storage.objects.filter(id = storage_id).first()

    return render(request, 'main_app/product_register.html', {'form1': form1, 'formResource':formResource ,'storage': storage})#para a pagina do armazem

@login_required
def product(request, storage_id):
    storage = Storage.objects.get(id=storage_id)

    content = Product.objects.all().filter(storage=storage)
    context = {
        'products': content,
        'storage': storage
    }
    return render(request, 'main_app/product.html', context)

@login_required
def product_edit(request, product_id):

    if request.method == 'POST':
        u_form = ProductUpdateForm(request.POST, request.FILES)
        if u_form.is_valid():
            product = Product.objects.get(id = product_id)
            storage = product.storage
            #encontrar o storage id e redirect
            u_form = ProductUpdateForm(request.POST, request.FILES, instance = product)
            u_form.save()
            messages.success(request, f'O seu produto foi atualizado!')
            storage_id = product.storage.id
            return redirect('storage_detail', pk=storage_id)

    else:

        product = Product.objects.filter(id = product_id).first()
        u_form = ProductUpdateForm(instance=product)

    product = Product.objects.filter(id = product_id).first()
    storage = product.storage

    return render(request, 'main_app/product_edit.html', {'u_form': u_form, 'storage': storage, 'product': product})

def searchbar(request):
    if request.method == 'GET':
        query = request.GET.get('query')
        if query:
            #category = Category.objects.filter(name = query).first()
            #subcategory = Subcategory.objects.filter(name = query).first()
            allCategory = Category.objects.all()
            allSubCategory = Subcategory.objects.all()
            allProducts = Product.objects.all()
            for prod in allProducts:
                if prod.title.lower() == query.lower():
                    selectedProduct = prod.title
                    break
                else:
                    selectedProduct = False
            for cat in allCategory:
                if cat.name.lower() == query.lower():
                    selectedCategory = cat
                    break
                else:
                    selectedCategory = False
            for subcat in allSubCategory:
                if subcat.name.lower() == query.lower():
                    selectedSubCategory = subcat
                    break
                else:
                    selectedSubCategory = False
            
            if selectedCategory and selectedSubCategory and selectedProduct:
                product = Product.objects.all().filter(Q(productcategory=selectedCategory.id) | Q(productsubcategory=selectedSubCategory.id) | Q(title=selectedProduct))
            elif selectedCategory and selectedSubCategory:
                product = Product.objects.all().filter(Q(productcategory=selectedCategory.id) | Q(productsubcategory=selectedSubCategory.id))
            elif selectedSubCategory and selectedProduct:
                product = Product.objects.all().filter(Q(productcategory=selectedSubCategory.id) | Q(title=selectedProduct))
            elif selectedCategory and selectedProduct:
                product = Product.objects.all().filter(Q(productcategory=selectedCategory.id) | Q(title=selectedProduct))
            elif selectedCategory:
                product = Product.objects.all().filter(Q(productcategory=selectedCategory.id))
            elif selectedSubCategory:
                product = Product.objects.all().filter(Q(productsubcategory=selectedSubCategory.id))
            elif selectedProduct:
                product = Product.objects.all().filter(Q(title=selectedProduct))
            else:
                product = False
                query=False


            return render(request, 'main_app/searchbarResult.html',{'products':product})
        else:
            print("Não existe informação para mostrar")
    return render(request, 'main_app/searchbarResult.html', {})


class OrderSummaryView(View):
    def get(self,request):
        if self.request.user.is_authenticated:
            try:
                order = Order.objects.get(user=self.request.user, complete=False)
                context = {
                    'objects': order
                }
                return render(self.request,'main_app/cart.html', context)
                
            except ObjectDoesNotExist:
                messages.error(self.request, "Nao existe uma encomenda")
                return redirect("/")
        else:
            try:
                order = Order.objects.get(session_id=request.session['nouser'], complete=False)
                context = {
                    'objects': order
                }
                return render(self.request,'main_app/cart.html', context)
                
            except:
                request.session['nouser'] = str(uuid.uuid4())
                order = Order.objects.create(session_id=request.session['nouser'], complete=False)
                return redirect("/")
   

def add_to_cart(request, slug):
    if request.user.is_authenticated:
        item = get_object_or_404(Product, slug=slug)
        order_item, created = OrderItem.objects.get_or_create(item= item, user=request.user, complete=False)
        order_qs= Order.objects.filter(user=request.user, complete=False)
        if order_qs.exists():
            order = order_qs[0]
            #check if order item is in the order
            if order.items.filter(item__slug=item.slug).exists():
                order_item.amount +=1
                order_item.save()
                messages.success(request, 'A quantidade do produto foi atualizada com sucesso!')
                return redirect("cart")
            else:
                order.items.add(order_item)
                messages.success(request, 'O seu produto foi adicionado ao carrinho com sucesso!')
                return redirect("cart")
        else:
            order = Order.objects.create(user=request.user)
            order.items.add(order_item)
            return redirect("cart")
    #user sem ter feito login
    else:
        try:
                order = Order.objects.get(session_id=request.session['nouser'], complete=False)
        except:
                request.session['nouser'] = str(uuid.uuid4())
        item = get_object_or_404(Product, slug=slug)
        order_item, created = OrderItem.objects.get_or_create(item= item,session_id=request.session['nouser'], complete=False)
        order_qs= Order.objects.filter(session_id=request.session['nouser'],complete=False)
        if order_qs.exists():
            order = order_qs[0]
            #check if order item is in the order
            if order.items.filter(item__slug=item.slug).exists():
                order_item.amount +=1
                order_item.save()
                messages.success(request, 'A quantidade do produto foi atualizada com sucesso!')
                return redirect("cart")
            else:
                order.items.add(order_item)
                messages.success(request, 'O seu produto foi adicionado ao carrinho com sucesso!')
                return redirect("cart")
        else:
            order = Order.objects.create()
            order.items.add(order_item)
            return redirect("cart")


def remove_from_cart(request,slug):
    if request.user.is_authenticated:
        item = get_object_or_404(Product, slug=slug)
        order_qs= Order.objects.filter(user=request.user, complete=False)
        if order_qs.exists():
            order = order_qs[0]
            #check if order item is in the order
            if order.items.filter(item__slug=item.slug).exists():
                order_item = OrderItem.objects.filter(item= item, user=request.user, complete=False)[0]
                order.items.remove(order_item)
                messages.success(request, 'O produto foi removido do carrinho com sucesso')
                return redirect("cart")
            else:
                messages.error(request, 'O produto que pretende remover nao existe no carrinho')
                return redirect("cart")
        else:
            messages.error(request, 'Nao existe nada no carrinho para remover')
            return redirect("cart")
    #user sem ter feito login
    else:  
        item = get_object_or_404(Product, slug=slug)
        order_qs= Order.objects.filter(session_id=request.session['nouser'], complete=False)
        if order_qs.exists():
            order = order_qs[0]
            #check if order item is in the order
            if order.items.filter(item__slug=item.slug).exists():
                order_item = OrderItem.objects.filter(item= item, session_id=request.session['nouser'], complete=False)[0]
                order.items.remove(order_item)
                messages.success(request, 'O produto foi removido do carrinho com sucesso')
                return redirect("cart")
            else:
                messages.error(request, 'O produto que pretende remover nao existe no carrinho')
                return redirect("cart")
        else:
            messages.error(request, 'Nao existe nada no carrinho para remover')
            return redirect("cart")


#Remover so um produto do carrinho carregando do simbolo de menos
def remove_one_item_cart(request,slug):

    if request.user.is_authenticated:
        item = get_object_or_404(Product, slug=slug)
        order_qs= Order.objects.filter(user=request.user, complete=False)
        if order_qs.exists():
            order = order_qs[0]
            #check if order item is in the order
            if order.items.filter(item__slug=item.slug).exists():
                order_item = OrderItem.objects.filter(item= item, user=request.user, complete=False)[0]
                order_item.amount -=1
                order_item.save()
                if order_item.amount < 1:
                    order.items.remove(order_item)
                    messages.success(request, 'O produto foi removido do carrinho com sucesso')
                    return redirect("cart")
                else:
                    messages.success(request, 'Uma unidade do produto foi removida do carrinho com sucesso')
                    return redirect("cart")
            else:
                messages.error(request, 'O produto que pretende remover nao existe no carrinho')
                return redirect("cart", slug=slug)
        else:
            messages.error(request, 'Nao existe nada no carrinho para remover')
            return redirect("cart", slug=slug)
    #user sem ter feito login
    else:
        item = get_object_or_404(Product, slug=slug)
        order_qs= Order.objects.filter(session_id=request.session['nouser'], complete=False)
        if order_qs.exists():
            order = order_qs[0]
            #check if order item is in the order
            if order.items.filter(item__slug=item.slug).exists():
                order_item = OrderItem.objects.filter(item= item, session_id=request.session['nouser'], complete=False)[0]
                order_item.amount -=1
                order_item.save()
                if order_item.amount < 1:
                    order.items.remove(order_item)
                    messages.success(request, 'O produto foi removido do carrinho com sucesso')
                    return redirect("cart")
                else:
                    messages.success(request, 'Uma unidade do produto foi removida do carrinho com sucesso')
                    return redirect("cart")
            else:
                messages.error(request, 'O produto que pretende remover nao existe no carrinho')
                return redirect("cart", slug=slug)
        else:
            messages.error(request, 'Nao existe nada no carrinho para remover')
            return redirect("cart", slug=slug)


#View do checkout
class CheckoutView(View):
    def get(self, *args, **kwargs):
        order = Order.objects.get(user=self.request.user, complete=False)
        form =  CheckoutForm()
        context = {
            'form':form,
            'order': order
        }
        return render(self.request, 'main_app/checkout.html', context)

    def post(self, *args, **kwargs):
        form = CheckoutForm(self.request.POST or None)
        if form.is_valid():
            print("Eh valido")
            return redirect('checkout')

#view da pagina de pagamento

def payment(request):

    return render(request, 'main_app/payment.html')

def order_status(request):
    lst=[]
    order_qs = OrderItem.objects.all()
    for i in order_qs:
        if i.item.storage.provider == request.user:
            lst.append(i) 
    context = {
        'orders': lst,
    }
    return render(request, 'main_app/orders_status.html', context)

#__________________________________________transports______________
@login_required
def base_register(request):
    if request.method == 'POST':
        user = request.user
        address  = request.POST.get("address")
        userLogged = CustomUser.objects.get(username=user)

        form1 = BaseRegisterForm(request.POST)
        if form1.is_valid():
            TransporterBase.objects.create(transporter=userLogged, address=address)
            messages.success(
                request, f'Base criada com sucesso! Já podes adicionar veículos a esta base.')
            return redirect('base')#para a pagina do armazem
    else:
        form1 = BaseRegisterForm()
    return render(request, 'main_app/base_register.html', {'form1': form1})#para a pagina do armazem """ """



@login_required
def base_transport(request):
    content = TransporterBase.objects.all().filter(transporter=request.user)
    context = {
        'transporterbase': content
    }
    return render(request, 'main_app/base_transport.html', context)


class base_detail(DetailView):
    model = TransporterBase
    def get_context_data(self, **kwargs):
        transporterbase = self.get_object()
        context = super().get_context_data(**kwargs)
        context["vehicle"] = Vehicle.objects.all().filter(base=transporterbase.id)

        return context

@login_required
def base_edit(request, base_id):

    if request.method == 'POST':
        #base  = request.POST.get("base.address")
        u_form = TransporterBaseUpdateForm(request.POST)
        if u_form.is_valid():
            base = TransporterBase.objects.get(id = base_id)
            u_form = TransporterBaseUpdateForm(request.POST, instance = base)
            u_form.save()
            messages.success(request, f'O sua base foi atualizada!')
            return redirect('base')

    else:

        base = TransporterBase.objects.filter(id = base_id).first()
        u_form = TransporterBaseUpdateForm(instance=base)


    return render(request, 'main_app/base_edit.html', {'u_form': u_form})


@login_required
def delete_base(request, base_id):
    if request.method == 'POST':
        
        base = TransporterBase.objects.get(id = base_id)
        delete_form = BaseDeleteForm(request.POST, instance = base)
        if delete_form.is_valid():
            base.delete()
            messages.info(request, 'O armazém foi apagado!')
            return redirect('storage')
    else:
        base = TransporterBase.objects.filter(id = base_id).first()

        delete_form = BaseDeleteForm(instance=base)

    context = {
        'delete_form': base.address
    }

    return render(request, 'main_app/delete_storage.html', context)

@login_required
def vehicle_register(request, base_id):
    if request.method == 'POST':
        base = TransporterBase.objects.get(id = base_id)

        form1 = VehicleRegisterForm(request.POST)

        if form1.is_valid():
            vehicle = form1.save(commit=False)
            vehicle.base = base
            vehicle.save()

            messages.success(request, f'Veículo foi criado com sucesso!')
            return redirect('transporterbase_detail', pk=base_id)#para a pagina do armazem
    else:
        form1 = VehicleRegisterForm()

    base = TransporterBase.objects.filter(id = base_id).first()

    return render(request, 'main_app/vehicle_register.html', {'form1': form1, 'transporterbase': base})#para a pagina do armazem
@login_required
def vehicle_edit(request, vehicle_id):

    if request.method == 'POST':

        u_form = StorageUpdateForm(request.POST)
        if u_form.is_valid():
            vehicle = Vehicle.objects.get(id = vehicle_id)
            u_form = VehicleUpdateForm(request.POST, instance = vehicle)
            u_form.save()
            messages.success(request, f'O seu veículo foi atualizado!')
            base_id = vehicle.base.id
            return redirect('transporterbase_detail', pk = base_id)

    else:

        vehicle = Vehicle.objects.filter(id = vehicle_id).first()
        u_form = VehicleUpdateForm(instance=vehicle)


    return render(request, 'main_app/vehicle_edit.html', {'u_form': u_form, 'vehicle': vehicle})

@login_required
def delete_vehicle(request, vehicle_id):
    if request.method == 'POST':
        vehicle = Vehicle.objects.get(id = vehicle_id)
        delete_form = VehicleDeleteForm(request.POST, instance=vehicle)
        if delete_form.is_valid():
            base_id = vehicle.base.id
            vehicle.delete()
            messages.info(request, 'O veículo foi apagado!')

            return redirect('transporterbase_detail', pk= base_id)
    else:
        vehicle = Vehicle.objects.filter(id = vehicle_id).first()
        delete_form = VehicleDeleteForm(instance=vehicle)

    context = {
        'vehicle': vehicle
    }

    return render(request, 'main_app/delete_vehicle.html', context)

